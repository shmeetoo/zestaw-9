#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "moduly.h"

// Delete last element of a list (tail)
void pop_back(struct Client **head) 
{
    if((*head)->next != NULL) // there is only head in this list
    {
        *head = NULL; // so delete this head
    }
    else // there are more elements
    {
        struct Client *somebody = *head;
        while(somebody->next->next != NULL) // till we reach the last element
        {
            somebody = somebody->next; // go to the next element
        }        

        free(somebody->next); // delete last element
        somebody->next = NULL; // set pointer of new last elem to NULL
    }
    return;
}
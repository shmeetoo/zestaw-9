CC=gcc

zad1: zad1.o list_size.o pop_back.o pop_by_index.o pop_by_surname.o pop_front.o push_back.o push_by_index.o push_front.o show_list.o
	$(CC) -Wall -o zad1 zad1.o list_size.o pop_back.o pop_by_index.o pop_by_surname.o pop_front.o push_back.o push_by_index.o push_front.o show_list.o
zad1.o: zad1.c moduly.h
	$(CC) -Wall -c zad1.c
list_size.o: list_size.c
	$(CC) -Wall -c list_size.c
pop_back.o: pop_back.c
	$(CC) -Wall -c pop_back.c
pop_by_index.o: pop_by_index.c 
	$(CC) -Wall -c pop_by_index.c 
pop_by_surname.o: pop_by_surname.c 
	$(CC) -Wall -c pop_by_surname.c 
pop_front.o: pop_front.c 
	$(CC) -Wall -c pop_front.c 
push_back.o: push_back.c 
	$(CC) -Wall -c push_back.c 
push_by_index.o: push_by_index.c 
	$(CC) -Wall -c push_by_index.c 
push_front.o: push_front.c 
	$(CC) -Wall -c push_front.c 
show_list.o: show_list.c 
	$(CC) -Wall -c show_list.c 
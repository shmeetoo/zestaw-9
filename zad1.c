#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "moduly.h"

void ilosc_klientow(struct Client *head) 
{
    int rozmiar = list_size(head);
    printf("\nLiczba klientow: %d\n", rozmiar);
    return;
}

void wyswietlanie(struct Client *head)
{
    show_list(head);
    return;
}

void dodawanie(struct Client *head)
{
    char surname[20] = "";
    int ile = 0;
    printf("\nPodaj ilu klientow chcialbys dodac: ");
    scanf("%d", &ile);

    for(int i=0; i<ile; i++)
    {
        printf("\nPodaj nazwiska klientow: ");
        scanf("%s", surname);
        push_back(&head, surname);
    }    
    return;
}

void usuwanie(struct Client *head)
{
    char surname[20] = "";
    printf("\nPodaj nazwisko klienta ktorego chcesz usunac: ");
    scanf("%s", surname);

    pop_by_surname(&head, surname);
    return;
}

int main()
{
    struct Client *head;
    head = (struct Client*)malloc(sizeof(struct Client));
    head = NULL;

    int opcja = 0;

    do {
    printf("Wybierz co chcesz zrobic:\n0. Zamkniecie programu\n1. Dodanie klienta\n2. Usuniecie klienta\n3. Wyswietlanie ilosci klientow\n4. Wyswietlenie nazwisk klientow\n");
    scanf("%d", &opcja);
    
    switch (opcja)
    {
        case 1:
            dodawanie(head);
            break;
        case 2:
            usuwanie(head);
            break;
        case 3:
            ilosc_klientow(head);
            break;
        case 4:
            wyswietlanie(head);
            break;
    }
    } while (opcja != 0);
    return 0;
}
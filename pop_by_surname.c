#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "moduly.h"


void pop_by_surname(struct Client **head, char *p_surname)
{
    int count = 0;

	struct Client* poprzedni=NULL;
    struct Client* wsk= *head;

    while((wsk != NULL) && (strcmp(wsk->surname, p_surname )!=0) )
    {
        poprzedni=wsk;
        wsk=wsk->next;
    }
    if(wsk == NULL )
        count = 0;
    else
    {
        if(poprzedni==NULL)
		{
        	(*head)=(*head)->next;
        	free(wsk);
   	 	}
    	else
    	{
        	poprzedni->next=wsk->next;
        	free(wsk);
   		}
   	 	count = 1;
    }

    if(count == 1)
        printf("Klient zostal usunieta");
    else
        printf("Klient nie zostal usuniety, poniewaz nie istnieje");

    return;
}